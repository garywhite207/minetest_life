# minetest_life

Minetest Life subgame
by Gary White (garywhite207)

A copy of the minetest_game subgame, but making it more like real-life

Minetest can be downloaded at http://minetest.net
Mods used:

default minetest_game mods by all the developers of Minetest
drinks by NathanS21
farming_plus by PilzAdam
moreblocks by Calinou
pipeworks and unified_inventory by VanessaE


All mods are still used under the license of their original release. Please consult the license file in each mod's files for each mod's individual license.
